#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h> 

#include "simplc.c"

int cursposit = 0; // cursor position
char errstr[256];

struct itimerval nval, oval; // global vars for timer

// INITIALIZE TIMER
void timer_init()
{
    nval.it_interval.tv_sec = 0;
    nval.it_interval.tv_usec = 100000;
    nval.it_value.tv_sec = 0;
    nval.it_value.tv_usec = 100000;
}

// BIT INVERSION
int switchbit(const int value, const int position) {
    return ((value ^ (1 << position)) & 65535);
}

int is_positive(int num) 
{
    if ((num & 65535) < 32768)
        return 0;
    return 1;
}


// OUTPUT
void print(){


    mt_gotoXY(1, 1);

    mt_clrscr();
    mt_setfgcolor(C_GREEN);


	//boxes
	bc_box(1, 1, 12, 62); // main frame
	bc_box(13, 1, 22, 44); // big number
	bc_box(13, 46, 22, 83); // keys
	bc_box(1, 63, 3, 83); // accumulator frame
	bc_box(4, 63, 6, 83); // instr. counter frame
	bc_box(7, 63, 9, 83); // operations frame
	bc_box(10, 63, 12, 83); // flags frame

    mt_setfgcolor(C_YELLOW);
	
	//name block
	mt_gotoXY(30, 1);	
	printf(" Memory ");
	mt_gotoXY(66, 1);
	printf(" accumulator ");
	mt_gotoXY(64, 4);
	printf(" instuctionCounter ");
	mt_gotoXY(68, 7);
	printf(" Operation ");
	mt_gotoXY(68, 10);
	printf(" Flags ");
	mt_gotoXY(48, 13);
	printf(" Keys: ");	
	
    mt_setfgcolor(C_RED);
	//accumulator
	mt_gotoXY(70, 2);
	printf("%04X", accum & 65535);
	
	//incounter
	mt_gotoXY(70, 5);	
	printf("%04X",  INCOUNTER);
	
	//operation 
	mt_gotoXY(68, 8);
	int com, c, o;
	com = sc_commandDecode(RAM[cursposit], &c, &o);
	com = RAM[cursposit] & 32768;
	if(com == 0)
	    printf("%02X : %02X", c, o);
	else
		printf("(number)");
	
	//flags
	mt_gotoXY(68, 11);
	int i;
	sc_regGet(overflow, &i);
	if (i == 1)
		printf("O ");
	else
		printf(" ");
	sc_regGet(division_by0, &i);
	if (i == 1)
		printf("0 ");
	else
		printf(" ");
	sc_regGet(out_of_memory, &i);	
	if (i == 1)
		printf("M ");
	else
		printf(" ");
	sc_regGet(ignoring_takt_impuls, &i);	
	if (i == 1)
		printf("T ");
	else
		printf(" ");
	sc_regGet(incorrect_command, &i);	
	if (i == 1)
		printf("E");
	else
		printf(" ");
	
	//keys
	mt_gotoXY(48, 14);
	printf("l  - load");
	mt_gotoXY(48, 14 + 1);
	printf("s  - save");
	mt_gotoXY(48, 14 + 2);
	printf("r  - run");
	mt_gotoXY(48, 14 + 3);
	printf("t  - step");
	mt_gotoXY(48, 14 + 4);
	printf("i  - reset");
	mt_gotoXY(48, 14 + 5);
	printf("F5 - accumulator");
    mt_gotoXY(48, 14 + 6);
	printf("F6 - instructionCounter");
	
    // RAM
    mt_setfgcolor(C_WHITE);	
    mt_gotoXY(2, 2);
    for (i = 0; i < 100; i++) {
        if (i % 10 == 0 && i > 0)
            mt_gotoXY(2, 2 + i / 10);

        if (i == INCOUNTER) {
            mt_setfgcolor(C_RED);
        }

        if (i == cursposit) { // selected element
            mt_setbgcolor(C_BLUE);
            mt_setfgcolor(C_MAGENTA);
        }
        if ((RAM[i] & 32768) == 0)
            printf("+");
        else
            printf(" ");
        printf("%04X", RAM[i]  & 65535);
        mt_setbgcolor(C_BLACK);
        mt_setfgcolor(C_WHITE);
        printf(" ");
    }

    // BIG RAM ELEMENT
    int arr[5][2];
    arr[0][0] = PLUS_BEGIN;
    arr[0][1] = PLUS_END;
    mt_setfgcolor(C_CYAN);

    if ((RAM[cursposit] & 32768) == 0)
        bc_printbigchar(arr[0], 14, 2);
    
    int wat[4];
    wat[3] = RAM[cursposit] % 16;
    wat[2] = (RAM[cursposit] / 16) % 16;
    wat[1] = (RAM[cursposit] / 256) % 16;
    wat[0] = (RAM[cursposit] / 4096);

    for (i = 0; i < 4; i++) { // digits
        if (wat[i] == 0) {arr[i + 1][0] = ZERO_BEGIN; arr[i + 1][1] = ZERO_END;}
        if (wat[i] == 1) {arr[i + 1][0] = ONE_BEGIN; arr[i + 1][1] = ONE_END;}
        if (wat[i] == 2) {arr[i + 1][0] = TWO_BEGIN; arr[i + 1][1] = TWO_END;}
        if (wat[i] == 3) {arr[i + 1][0] = THREE_BEGIN; arr[i + 1][1] = THREE_END;}
        if (wat[i] == 4) {arr[i + 1][0] = FOUR_BEGIN; arr[i + 1][1] = FOUR_END;}
        if (wat[i] == 5) {arr[i + 1][0] = FIVE_BEGIN; arr[i + 1][1] = FIVE_END;}
        if (wat[i] == 6) {arr[i + 1][0] = SIX_BEGIN; arr[i + 1][1] = SIX_END;}
        if (wat[i] == 7) {arr[i + 1][0] = SEVEN_BEGIN; arr[i + 1][1] = SEVEN_END;}
        if (wat[i] == 8) {arr[i + 1][0] = EIGHT_BEGIN; arr[i + 1][1] = EIGHT_END;}
        if (wat[i] == 9) {arr[i + 1][0] = NINE_BEGIN; arr[i + 1][1] = NINE_END;}
        if (wat[i] == 0xA) {arr[i + 1][0] = A_BEGIN; arr[i + 1][1] = A_END;}
        if (wat[i] == 0xB) {arr[i + 1][0] = B_BEGIN; arr[i + 1][1] = B_END;}
        if (wat[i] == 0xC) {arr[i + 1][0] = C1_BEGIN; arr[i + 1][1] = C1_END;}
        if (wat[i] == 0xD) {arr[i + 1][0] = D_BEGIN; arr[i + 1][1] = D_END;}
        if (wat[i] == 0xE) {arr[i + 1][0] = E_BEGIN; arr[i + 1][1] = E_END;}
        if (wat[i] == 0xF) {arr[i + 1][0] = F_BEGIN; arr[i + 1][1] = F_END;}

        bc_printbigchar(arr[i + 1], 14, 2 + 8 * (i + 1));
    }

    mt_gotoXY(1, 24);

    mt_setbgcolor(C_BLACK); // default colors
    mt_setfgcolor(C_WHITE);	

    printf("%s\n", errstr);
}


// READ FUNCTION
int get_num(int *n)
{
	char buf[256];
	
    sprintf(errstr, "Enter the value:");
    print();

    fflush(stdin);
	fgets(buf, 256, stdin);

	if (sscanf(buf, "%x", n) != 1)
		return -1;
	return 0;
}


// ALU
int ALU(int command, int operand){
	switch(command){
		case 0x30:{//ADD
			accum += (switchbit(RAM[operand], 15) & 32767);
		} break;
		case 0x31:{//SUB
			accum -= (switchbit(RAM[operand], 15) & 32767);
		} break;
		case 0x32:{//DIVIDE
			if(RAM[operand] == 0 ){
				sc_regSet(division_by0, 1);
				return 1;				
			}
			accum /= (switchbit(RAM[operand], 15) & 32767);
		} break;
		case 0x33:{//MUL
			accum *= (switchbit(RAM[operand], 15) & 32767);
		} break;

        case 0x68: { // LOGRC
            accum = (RAM[operand] & 32767) >> accum;
        } break;
	}
	return 0;
}


// CU
int CU(){

    accum &= 65535;

	int command, operand, t = INCOUNTER;

	if ((RAM[INCOUNTER] & 32768) == 1){ // number instead of command
		sc_regSet(incorrect_command, 1);
		sc_regSet(ignoring_takt_impuls, 1);
        sprintf(errstr, "Not a command!");
       	return 1;
	}	

	int tmp = sc_commandDecode(RAM[INCOUNTER], &command, &operand);
	if(tmp == 1){ // incorrect command
		sc_regSet(incorrect_command, 1);
        sprintf(errstr, "Not a command!");
		return 1;	
	}	
	
	if(operand >= 100 || operand < 0){ //reference to non-existing operand
		sc_regSet(out_of_memory, 1);
		sc_regSet(ignoring_takt_impuls, 1);

        sprintf(errstr, "Out of memory!");
        print();

       	return 1;
	}	
	
	switch(command){
		case 0x10:{//READ
			operation1 = 10;
			operation2 = operand;
			
            int p, n = 0;
            setitimer(ITIMER_REAL, NULL, NULL);			

            sprintf(errstr, "Enter the value:");
            print();
			if(!scanf("%d", &n) || n > 16383 || n < 0){
				sc_regSet(overflow, 1);
				sc_regSet(ignoring_takt_impuls, 1);
                sprintf(errstr, "Error!");
                print();
       			return 1;
			}				

			RAM[operand] = n | 32768;
            sc_regGet(ignoring_takt_impuls, &p);

            if (p == 0)
                setitimer(ITIMER_REAL, &nval, &oval);    						
            sprintf(errstr, " ");
	        print();		
		} break;
		case 0x11:{//WRITE
			operation1 = 0x11;
			operation2 = operand;
            if (RAM[operand] < -0x8000) { // command
    			sprintf(errstr, "+%X", RAM[operand]);			
            } else { // number
                if (RAM[operand] < 0x8000)
                    sprintf(errstr, "%d", -(32768 - (RAM[operand] & 32767)));
                else
                    sprintf(errstr, "%d", RAM[operand] & 32767);
            }

		} break;
        case 0x20:{//LOAD
            operation1 = 0x20;
            operation2 = operand;
            if (is_positive(RAM[operand]))
                accum = switchbit(RAM[operand], 15) & 32767;
            else
                accum = switchbit(RAM[operand], 15) | 32768;
            print();
        } break;
        case 0x21:{//STORE
            operation1 = 0x21;
            operation2 = operand;
            if (accum > 0x4000)
                RAM[operand] = switchbit(accum, 15);
            else
                RAM[operand] = switchbit(accum, 15) | 32768 ;
            print();
        } break;
		case 0x30:{//ADD
			operation1 = 0x30;
			operation2 = operand;
			tmp = ALU(command, operand);	
			if(tmp == 1){	
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
		} break;
		case 0x31:{//SUB
			operation1 = 0x31;
			operation2 = operand;
			tmp = ALU(command, operand);	
			
			if(tmp == 1){	
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
		} break;
		case 0x32:{//DIVIDE
			operation1 = 0x32;
			operation2 = operand;
			tmp = ALU(command, operand);	
			
			if(tmp == 1){	
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
		} break;
		case 0x33:{//MUL
			operation1 = 0x33;
			operation2 = operand;
			tmp = ALU(command, operand);	
			
			if(tmp == 1){	
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
		} break;
		case 0x40:{//JUMP
			operation1 = 0x40;
			operation2 = operand;
			INCOUNTER = operand;			
            print();
		} break;
		case 0x41:{//JNEG
			operation1 = 0x41;
			operation2 = operand;        
			if((accum & 65535)> 0x4000)
				INCOUNTER = operand;			
            print();
		} break;
		case 0x42:{//JZ
			operation1 = 0x42;
			operation2 = operand;
			if(accum == 0)
				INCOUNTER = operand;			
            print();
		} break;
		case 0x43:{//HALT
			operation1 = 0x43;
			operation2 = 0;
            setitimer(ITIMER_REAL, NULL, NULL);
            sc_regSet(ignoring_takt_impuls, 1);
			t = -1;
            print();
		} break;
	
		case 0x55:{//JNS
			operation1 = 0x55;
			operation2 = operand;
			if(accum > 0)
				INCOUNTER = operand;			
            print();
		} break;

        case 0x68: { // LOGRC
            operation1 = 0x68;
            operation2 = operand;
            tmp = ALU(command, operand);
			if(tmp == 1){
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
        } break;
        case 0x70: { // RCCR
            operation1 = 0x70;
            operation2 = operand;
            tmp = ALU(command, operand);
			if(tmp == 1){
				sc_regSet(ignoring_takt_impuls, 1);
				return 1;	
			}	
            print();
        } break;
		default:{
			sprintf(errstr, "Command %02X is not supported by this version\n", command);
			sc_regSet(ignoring_takt_impuls, 1);
            return 0;
		} break;
	}
	if(t == INCOUNTER)	
		INCOUNTER++;
	return 0;	
}


// SIGNAL HANDLER
void sighandler(int sig)
{
    if (sig == SIGALRM) { // signal from timer
    	CU();
        print();
        int tmp;
        sc_regGet(ignoring_takt_impuls, &tmp);
    	if (!tmp) {
    		//alarm(1);
        }
    }
    if (sig == SIGUSR1) { // user-defined signal
    	alarm(0);
    	sc_regInit();
    	sc_regSet(ignoring_takt_impuls, 1);
    	INCOUNTER = 0;
        accum = 0;
    }
}

// MAIN FUNCTION
int main(int argc, char **argv)
{	

    sc_regInit();
    sc_memoryInit();

    // signal init
    signal(SIGALRM, sighandler);
    signal(SIGUSR1, sighandler);

    timer_init();	

    INCOUNTER = 0;	
    sc_regSet(ignoring_takt_impuls, 1);

    enum keys ch = 0;
    
    setbuf(stdout, NULL); 

    char fname[256];

    int kill = 0;
    int ntrpt = -1;

    while (!kill) {

        print(); 
        
        sprintf(errstr, " ");      
        ch = K_OTHER;
        rk_readkey(&ch);  

        sc_regGet(overflow, &ntrpt);
        if (ntrpt) setitimer(ITIMER_REAL, NULL, NULL);
        sc_regGet(division_by0, &ntrpt);
        if (ntrpt) setitimer(ITIMER_REAL, NULL, NULL);
        sc_regGet(out_of_memory, &ntrpt);
        if (ntrpt) setitimer(ITIMER_REAL, NULL, NULL);
        sc_regGet(incorrect_command, &ntrpt);
        if (ntrpt) setitimer(ITIMER_REAL, NULL, NULL);

        sc_regGet(ignoring_takt_impuls, &ntrpt);


        if (ntrpt) { // if clock pulses are ignored
            setitimer(ITIMER_REAL, NULL, NULL);
            switch(ch) {
                case K_LEFT: if (cursposit % 10 > 0) cursposit--; else cursposit += 9; break; // left
                case K_UP: if (cursposit >= 10) cursposit -= 10; else cursposit = 90 + cursposit % 10; break; // up
                case K_RIGHT: if (cursposit % 10 < 9) cursposit++; else cursposit -= 9; break; // right
                case K_DOWN: if (cursposit <= 89) cursposit += 10; else cursposit = cursposit % 10; break; // down
                case K_l: { // load memory from file
                	printf("Enter filename: ");
                	fgets(fname, 256, stdin);
                	fname[strlen(fname) - 1] = '\0';
                	if (sc_memoryLoad(fname) == 0) {
                        sc_memoryLoad(fname);
                		sprintf(errstr, "File successfully loaded");
                        print();
                	}
                	else {
                		sprintf(errstr, "Cannot load file: %s", fname);
                	}
                    sprintf(errstr, " "); 
                } break;
                case K_s: { // save memory to file
                	printf("Enter filename: ");
                	fgets(fname, 256, stdin);
                	fname[strlen(fname) - 1] = '\0';
                	sc_memorySave(fname);
                    sprintf(errstr, " "); 
                } break;
                case K_t: { // timer step
                    sighandler(SIGALRM);
                    print();
                } break;
                case K_F5: { // accumulator
                   	int num;  
                	if (get_num(&num) != 0) {
                		sprintf(errstr, "Not a number!");
                		return -1;
                	}
                	if ((num >= 0) && (num < 0x8000)) {
                		accum = num;
                	}
                	else {
                		sprintf(errstr, "Accumutalor is 15 bit wide");
                	}
                    sprintf(errstr, " ");                     
                } break;
                case K_F6: { // instruction counter
                   	int num;
                    
                	if (get_num(&num) != 0) {
                		sprintf(errstr, "Not a number!");
                	}
                	if ((num >= 0) && (num < 0x64)) {
                		INCOUNTER = num;
                	}
                	else {
                		sprintf(errstr, "Value must be in range [0, 99]");
                	}
                    sprintf(errstr, " ");                     
                } break;
                case K_ENTER: { // memory cell
                	int num;
                 	if (get_num(&num) != 0) {
                		sprintf(errstr, "Not a number!");
                	}
                    sc_memorySet(cursposit, num);
                    sprintf(errstr, " "); 
                } break;
                default: {}
            }        

        }
        if (ch == K_q) { // quit
            kill = 1;
        }
        if (ch == K_i) { // reset
            raise(SIGUSR1);
            cursposit = 0;
        } 
        if (ch == K_r) {
            int treg = -1;
            sc_regGet(ignoring_takt_impuls, &treg);
            if (treg == 1) {
                sc_regSet(ignoring_takt_impuls, 0);
                setitimer(ITIMER_REAL, &nval, &oval);                
            } else {
                alarm(0);
                sc_regSet(ignoring_takt_impuls, 1);
                sprintf(errstr, " "); 
            }
        }
    }

    return 0;
}
