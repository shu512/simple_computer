#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ioctl.h> 
#include <fcntl.h>
#include <termios.h>
#include <string.h>

#include "simplc.h"
#include "myterm.h"
#include "mybigchar.h"
#include "myreadkey.h"

// INIT MEMORY WITH 0 VALUES
int sc_memoryInit()
{
    int i;
    for (i = 0; i < RAM_SIZE; i++)
        RAM[i] = 0;

    return 0;
}

// SET MEMORY CELL
int sc_memorySet(int address, int value)
{
    int flag = 0;
    if (address >= 0 && address < RAM_SIZE)
        RAM[address] = value;
    else
        flag = 1;
    sc_regSet(out_of_memory, flag);
    return flag;
}

// GET MEMORY CELL VALUE
int sc_memoryGet(int address, int *value)
{
    int flag = 0;
    if (address >= 0 && address < RAM_SIZE)
        *value = RAM[address];
    else
        flag = 1;
    sc_regSet(out_of_memory, flag);
    return flag;
}

// SAVE MEMORY TO FILE
int sc_memorySave(char *filename)
{
    FILE *f = NULL;
    f = fopen(filename, "wb");
    int flag = 0;

    if (f == NULL)
        flag = 1;
    else {
        fwrite(RAM, sizeof(int), RAM_SIZE, f);
        fclose(f);
    }
    return flag;
}

// LOAD MEMORY FROM FILE
int sc_memoryLoad(char *filename)
{
    FILE *f = NULL;
    f = fopen(filename, "rb");
    int flag = 0;

    if (f == NULL)
        flag = 1;
    else {
        fread(RAM, sizeof(int), RAM_SIZE, f);
        fclose(f);
    }

    if (flag == 1)
        printf("FILE READ ERROR!\n");
    
    return flag;
}

// INIT REGISTERS WITH 0 VALUES
int sc_regInit()
{
    REGISTER = 0;
    INCOUNTER = 0;
    return 0;
}

// SET REGISTER VALUE
int sc_regSet(int reg, int value)
{
    if (reg != overflow && reg != division_by0 && reg != out_of_memory && reg != ignoring_takt_impuls && reg != incorrect_command) {
        return 1;    
    }

    if (value == 0) {
        REGISTER &= ~(reg);
    } else {
        if (value == 1) {
            REGISTER |= reg;
        } else {
            return 1;
        }
    }
    return 0;
}

// GET REGISTER VALUE
int sc_regGet(int reg, int *value)
{
    int k = -1;
    switch(reg) {
        case overflow: k = 0; break;
        case division_by0: k = 1; break;
        case out_of_memory: k = 2; break;
        case ignoring_takt_impuls: k = 3; break;
        case incorrect_command: k = 4; break;
    }
    if (k == -1)
        return 1;
    *value = (REGISTER >> k) & 1;

    return 0;
}

// ENCODE COMMAND
int sc_commandEncode(int command, int operand, int *value)
{
    if (operand < 0 || operand > 255 || command < 0 || command > 127) {
        sc_regSet(out_of_memory, 1);
        return 1;
    }

    int tmp = 0;
    tmp |= (command << 8);
    tmp |= operand;
    *value = tmp;

    return 0;
}

// DECODE COMMAND
int sc_commandDecode(int value, int *command, int *operand)
{
    if (value > 0x8000 || value < 0) {
        sc_regSet(ignoring_takt_impuls, 1);
        sc_regSet(incorrect_command, 1);
        return 1;
    }
    int op = 255, com = 127;
    op &= value;
    com &= (value >> 8);
    
    *operand = op;    
    *command = com;

    return 0;
}

