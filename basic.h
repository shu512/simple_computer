#ifndef BASIC
#define BASIC



struct memory_t {
	int is_val;
	int command;
	int operand;
};

struct b_label {
	int label;
	int pos;
};

struct stack_t {
	char c;
	struct stack_t *next;
};

enum command {
	COM_REM = 1,
	COM_INPUT = 2,
	COM_OUTPUT = 3,
	COM_LET = 4,
	COM_IF = 5,
	COM_GOTO = 6,
	COM_END = 7,
};

#endif
